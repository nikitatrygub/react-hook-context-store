import React from "react";
import { todosStore } from "../containers/todos/store";

export const ListTodos = () => {
  const store = todosStore.useStore();
  const { state } = store;
  return (
    <div>
      {state.todos &&
        state.todos.map((todo, key) => <p key={key}>{todo.title}</p>)}
    </div>
  );
};
