import React, { useState } from "react";
import { useTodosStore, fetchTodos } from "../containers/todos/store";

export const FetchTodosButton = () => {
  const [isOpened , toggle] = useState(false);
  const store = useTodosStore();
  const { dispatch } = store;
  console.log(store.state)
  return (
    <div>
      <button onClick={() => toggle(!isOpened)}>Toggle: {isOpened ? "opened" : "closed"}</button>
      <button onClick={() => dispatch(fetchTodos, { id: 0 })}>
        Fetch todos
      </button>
    </div>
  );
};
