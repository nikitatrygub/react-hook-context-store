import React from "react";
import { useUsersStore } from "../containers/users/store";
import { fetchUser } from "../containers/users/actions";

export const FetchUserButton = () => {
  const store = useUsersStore();
  const { dispatch } = store;
  return <button onClick={() => dispatch(fetchUser, { id: 1 })}>Fetch</button>;
};
