import React from "react";
import { useUsersStore } from "../containers/users/store";

export const UserView = () => {
  const store = useUsersStore();
  const { state } = store;
  console.log(state);
  return <div>User {state.user && state.user.name}</div>;
};
