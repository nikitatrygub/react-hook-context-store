import React from "react";
import { Users } from "./containers/users";
import { Todos } from "./containers/todos";

function App() {
  return (
    <div className="App">
      <Users />
      <Todos />
    </div>
  );
}

export default App;
