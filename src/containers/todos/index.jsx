import React from "react";
import { FetchTodosButton } from "../../components/FetchTodosButton";
import { ListTodos } from "../../components/ListTodos";

export const Todos = () => {
    return (
        <div>
            <FetchTodosButton />
            <ListTodos />
        </div>
    )
}
