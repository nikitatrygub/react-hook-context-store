import { createStore } from "../../store";
import { produce } from "immer";

const initialState = {
  todos: null,
  isLoading: false
};

const todosReducer = (state, action) =>
  produce(state, draft => {
    switch (action.type) {
      case "LOADING": {
        draft.isLoading = true;
        return;
      }
      case "SUCCESS": {
        draft.isLoading = false;
        draft.todos = action.payload;
        return;
      }
      default:
        return;
    }
  });

export const fetchTodos = ({ id }) => {
  return async dispatch => {
    dispatch({
      type: "LOADING"
    });
    const resp = await fetch("https://jsonplaceholder.typicode.com/todos/");
    if (resp.ok) {
      const todos = await resp.json();
      dispatch({
        type: "SUCCESS",
        payload: todos
      });
    }
  
  };
};

export const todosStore = createStore(todosReducer, initialState, "todos");
export const useTodosStore = () => todosStore.useStore();
