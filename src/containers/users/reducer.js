import { produce } from "immer";

export const usersReducer = (state, action) =>
  produce(state, draft => {
    switch (action.type) {
      case "LOADING": {
        draft.isLoading = true;
        return;
      }
      case "SUCCESS": {
        draft.isLoading = false;
        draft.user = action.payload;
        return;
      }
      default:
        return;
    }
  });
