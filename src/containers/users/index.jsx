import React from "react";
import { UserView } from "../../components/UserView";
import { FetchUserButton } from "../../components/FetchUserButton";

export const Users = () => {
  return (
    <div>
      <UserView />
      <FetchUserButton />
    </div>
  );
};
