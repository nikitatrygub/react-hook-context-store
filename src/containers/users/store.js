import { usersReducer } from "./reducer";
import { createStore } from "../../store";

const initialState = {
  user: null,
  isLoading: false
};

export const usersStore = createStore(
  usersReducer,
  initialState,
  "users"
);

export const useUsersStore = () => usersStore.useStore();
