export const fetchUser = ({ id }) => {
  return async dispatch => {
    dispatch({
      type: "LOADING"
    });
    const resp = await fetch(
      `https://jsonplaceholder.typicode.com/users/${id}`
    );
    if (resp.ok) {
      const user = await resp.json();
      dispatch({
        type: "SUCCESS",
        payload: user
      });
    }
  };
};
