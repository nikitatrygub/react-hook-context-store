import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { createStoreProvider } from "./store";
import { usersStore } from "./containers/users/store";
import { todosStore } from "./containers/todos/store";

const StoreProvider = createStoreProvider([
  usersStore.users,
  todosStore.todos
]);

ReactDOM.render(
  <StoreProvider>
    <App />
  </StoreProvider>,
  document.getElementById("root")
);
