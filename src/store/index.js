import React, { useReducer } from "react";

export const createStoreProvider = providers => {
  return ({ children }) => {
    return providers
      .reverse()
      .reduce((tree, Provider) => <Provider>{tree}</Provider>, children);
  };
};

export const createStore = (reducer, initialState, name) => {
  const Context = React.createContext();
  const { Provider } = Context;

  const store = {
    [name]: ({ children }) => {
      const [state, dispatchAction] = useReducer(reducer, initialState);

      const dispatch = (action, args) => action(args)(dispatchAction);
      const value = { state, dispatch };
      return <Provider value={value}>{children}</Provider>;
    },
    useStore: () => React.useContext(Context)
  };

  return store;
};
